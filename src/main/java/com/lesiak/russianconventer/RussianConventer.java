package com.lesiak.russianconventer;

import com.lesiak.russianconventer.api.Conventer;
import com.lesiak.russianconventer.api.RussianToPolishConverter;

public class RussianConventer {

    public static void main(String[] args) {

        Conventer conventer = new RussianToPolishConverter();
        System.out.println(conventer.convert("Я не говорю на вашем языке."));

    }

}
