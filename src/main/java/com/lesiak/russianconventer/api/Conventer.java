package com.lesiak.russianconventer.api;

public interface Conventer {
    String convert(String word);
}
