package com.lesiak.russianconventer.api;

import java.util.HashMap;
import java.util.Map;

public class RussianToPolishConverter implements Conventer {
    private Map<String, String> lettersList = fillMapOfRussianLettersWithPolishTranscription();

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public String convert(String russianString) {
        String processedRussianWord;

        if (russianString.toLowerCase().contains("лb")) {
            processedRussianWord = russianString.replace("лb", "l");
        }
        if (russianString.toLowerCase().contains("зы")) {
            processedRussianWord = russianString.replace("зы", "zi");
        } else processedRussianWord = russianString;

        char[] charArray = processedRussianWord.toCharArray();
        StringBuilder word = new StringBuilder();

        for (char c : charArray
                ) {
            word.append(changeLetterToTranscript(c));
        }

        return word.toString();
    }

    private String changeLetterToTranscript(char letter) {
        String key = Character.toString(letter);
        if (lettersList.containsKey(key)) {
            return lettersList.get(key);
        } else
            return key;

    }

    private Map<String, String> fillMapOfRussianLettersWithPolishTranscription() {
        Map<String, String> rusToPolLetters = new HashMap<String, String>();

        rusToPolLetters.put("А", "a");
        rusToPolLetters.put("а", "a");
        rusToPolLetters.put("Б", "б");
        rusToPolLetters.put("б", "b");
        rusToPolLetters.put("В", "w");
        rusToPolLetters.put("в", "w");
        rusToPolLetters.put("Г", "g");
        rusToPolLetters.put("г", "g");
        rusToPolLetters.put("Д", "d");
        rusToPolLetters.put("д", "d");
        rusToPolLetters.put("Е", "je");
        rusToPolLetters.put("е", "je");
        rusToPolLetters.put("Ё", "jo");
        rusToPolLetters.put("ё", "jo");
        rusToPolLetters.put("Ж", "ż");
        rusToPolLetters.put("ж", "ż");
        rusToPolLetters.put("З", "że");
        rusToPolLetters.put("з", "że");
        rusToPolLetters.put("И", "i");
        rusToPolLetters.put("и", "i");
        rusToPolLetters.put("Й", "i");
        rusToPolLetters.put("й", "i");
        rusToPolLetters.put("К", "k");
        rusToPolLetters.put("к", "k");
        rusToPolLetters.put("Л", "ł");
        rusToPolLetters.put("л", "ł");
        rusToPolLetters.put("М", "m");
        rusToPolLetters.put("м", "m");
        rusToPolLetters.put("Н", "n");
        rusToPolLetters.put("н", "n");
        rusToPolLetters.put("О", "o");
        rusToPolLetters.put("о", "o");
        rusToPolLetters.put("П", "p");
        rusToPolLetters.put("п", "p");
        rusToPolLetters.put("Р", "r");
        rusToPolLetters.put("р", "r");
        rusToPolLetters.put("С", "s");
        rusToPolLetters.put("с", "s");
        rusToPolLetters.put("Т", "t");
        rusToPolLetters.put("т", "t");
        rusToPolLetters.put("У", "u");
        rusToPolLetters.put("у", "u");
        rusToPolLetters.put("Ф", "f");
        rusToPolLetters.put("ф", "f");
        rusToPolLetters.put("Х", "ch");
        rusToPolLetters.put("х", "ch");
        rusToPolLetters.put("Ц", "c");
        rusToPolLetters.put("ц", "c");
        rusToPolLetters.put("Ч", "cz");
        rusToPolLetters.put("ч", "cz");
        rusToPolLetters.put("Ш", "sz");
        rusToPolLetters.put("ш", "sz");
        rusToPolLetters.put("Щ", "szcz");
        rusToPolLetters.put("щ", "szcz");
        rusToPolLetters.put("Ы", "y");
        rusToPolLetters.put("ы", "y");
        rusToPolLetters.put("Э", "e");
        rusToPolLetters.put("э", "e");
        rusToPolLetters.put("Ю", "ju");
        rusToPolLetters.put("ю", "ju");
        rusToPolLetters.put("Я", "ja");
        rusToPolLetters.put("я", "ja");
        rusToPolLetters.put("Ъ", "j");
        rusToPolLetters.put("ъ", "j");
        rusToPolLetters.put("Ь", "~");
        rusToPolLetters.put("ь", "~");

        return rusToPolLetters;
    }

}
